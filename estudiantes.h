/*** estudiantes.h***/


#ifndef ESTUDIANTES_H
#define ESTUDIANTES_H

typedef struct {
	//char codigo[6];
	//char nombre[10];
	float proy1, proy2, proy3;
	float cont1, cont2, cont3, cont4, cont5, cont6;
} asignatura; 


typedef struct{
	char nombre[11];
	char apellidoP[11];
	char apellidoM[11];
	asignatura asig_1; 
	float prom;
} estudiante;


void cargarEstudiantes(char path[], estudiante curso[]); 

void grabarEstudiantes(char path[], estudiante curso[]);



#endif //ESTUDIANTES_H

