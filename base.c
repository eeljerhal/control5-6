#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
#include <math.h>

int const_array = 24;  //Cantidad de alumnos (basado en la funcion grabarEstudiantes de arch.c y probar menos cantidad dde estudiantes)
int validar_carga = 0; //Variable para validar si se cargo el archivo de estudiantes.

float calcularVarianza(float media, float prom[]){	//Funcion en la cual se calcula la varianza para el calculo de la desviacion estandar
	float varianza = 0.0;
	int i = 0;
	while(i < const_array){
		varianza = varianza + (pow((prom[i] - media), 2));
		i++;
	}
	return (varianza/((float)const_array-1));
}

float desvStd(float media, float prom[]){	//funcion que calcula la desviacion estandar, en donde se llama a la funcion de varianza para el calculo
	float desvstd;
	desvstd = calcularVarianza(media, prom);

	return sqrtf(desvstd);
}

float menor(int i, float nota_min, float prom[]){	//funcion que busca la menor nota de los promedios de los alumnos
	if (prom[i] < nota_min){
		return prom[i];
	}
	else{
		return nota_min;
	}

}

float mayor(int i, float nota_max, float prom[]){	//funcion que busca la mayor nota de los promedios de los alumnos
	if (prom[i] > nota_max){
		return prom[i];
	}
	else{
		return nota_max;
	}
}

void metricasEstudiantes(estudiante curso[]){	//funcion en donde se muestran por pantalla los resultados de los calculos de desviacion estandar y la menor/mayor nota de los promedios
	if(validar_carga == 1){	//Se valida que se cargo el archivo inicial de alumnos
		int i = 0;
		float prom[const_array], nota_min = 7.0, nota_max = 1.0, media = 0.0, desvstd;
		while(i < const_array){		//Se recorre el arreglo de alumnos en donde se obtiene la nota del promedio y se realizan las tareas correspondientes para cada resultado pedido
			prom[i] = curso[i].prom;
			nota_min = menor(i, nota_min, prom);
			nota_max = mayor(i, nota_max, prom);
			media = media + prom[i];

			i++;
		}
		media = (media / const_array);
		desvstd = desvStd(media, prom);
		printf("\nNota Mayor: %.1f\nNota Menor: %.1f\nDesviacion Estandar: %.1f\n", nota_max, nota_min, desvstd);
	
	}else{

		printf("\nCargue el archivo de estudiantes en la opcion 1.\n");
	}
}

int registroNotasProyectos(int i, float nota[], estudiante curso[]){	//funcion en la cual se ingresan solo las notas de los proyectos para cada alumno
	if(validar_carga == 1){	
		int nro_nota = 0, ingresar;
		printf("Ingrese las notas de los 3 proyectos para el alumno: %s %s %s.\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM);
		while(nro_nota < 3){
			ingresar = 1;
			while(ingresar == 1){	//Se ingresa una nota y se procede a validar si es una nota valida que en caso de no serla se pide reingreso de esta
				printf("Ingrese nota Proyecto %d:", nro_nota+1);
				scanf("%f", &nota[nro_nota]);
				if(nota[nro_nota] < 1.0 || nota[nro_nota] > 7.0){
					printf("Valor erroneo en las notas, reingrese\n");
					ingresar = 1;
				}else{
					ingresar = 0;
				}
			}
			nro_nota++;
		}
		return nro_nota;	//Se retorna la cantidad de notas (3) para la siguiente funcion
	}else{
		printf("\nCargue el archivo de estudiantes en la opcion 1.\n");
	}
}

void registroNotasControles(int nro_nota, float nota[], estudiante curso[]){	//funcion en donde se ingresan las notas de los controles de cada alumno
	int ingresar;
	printf("Ingrese las notas de los 6 controles:\n");
	while(nro_nota < 9){	//Se recibe el numero de notas para seguir agregando ordenadamente las notas al arreglo (p1,p2,p3,c1,c2,c3.....)
		ingresar = 1;
		while(ingresar == 1){
			printf("Ingrese nota Control %d:", nro_nota-2);
			scanf("%f", &nota[nro_nota]);
			if(nota[nro_nota] < 1.0 || nota[nro_nota] > 7.0){
				printf("Valor erroneo en las notas, reingrese\n");
				ingresar = 1;
			}else{
				ingresar = 0;
			}
		}
		nro_nota++;
	}
}

void almacenarNotasAlumnos(int i, float nota[], estudiante curso[]){	//funcion en donde se almacenan las notas ingresadas desde el arreglo de notas hacia el arreglo del tipo de datos de asignatura->estudiante
	curso[i].asig_1.proy1 = nota[0];
	curso[i].asig_1.proy2 = nota[1];
	curso[i].asig_1.proy3 = nota[2];
	curso[i].asig_1.cont1 = nota[3];
	curso[i].asig_1.cont2 = nota[4];
	curso[i].asig_1.cont3 = nota[5];
	curso[i].asig_1.cont4 = nota[6];
	curso[i].asig_1.cont5 = nota[7];
	curso[i].asig_1.cont6 = nota[8];
	curso[i].prom = ((nota[0] + nota[1])*0.2 + nota[2]*0.3 + (nota[3] + nota[4] + nota[5] + nota[6] + nota[7] + nota[8])*0.05);
}

void registroCurso(estudiante curso[]){	//funcion en donde se agregan las notas para cada alumno en donde se utilizan 3 funciones (registro notas proyectos, registro notas controles, almacenar notas alumnos)
	if(validar_carga == 1){
		int i = 0, nro_nota;
		float nota[9];
		while (i < const_array){	//Se recorre el arreglo para realizar el registro para cada alumno registrado
			nro_nota = registroNotasProyectos(i, nota, curso);
			registroNotasControles(nro_nota, nota, curso);
			almacenarNotasAlumnos(i, nota, curso);
					
			printf("Proyectos: %.1f - %.1f - %.1f Controles: %.1f - %.1f - %.1f - %.1f - %.1f - %.1f Promedio: %.1f\n", curso[i].asig_1.proy1, curso[i].asig_1.proy2, curso[i].asig_1.proy3, curso[i].asig_1.cont1, curso[i].asig_1.cont2, curso[i].asig_1.cont3, curso[i].asig_1.cont4, curso[i].asig_1.cont5, curso[i].asig_1.cont6, curso[i].prom);

			i++;
		}
	}else{
		printf("\nCargue el archivo de estudiantes en la opcion 1.\n");
	}
	
}

void ordenarNotas(int id[], int cant_notas, estudiante curso[]){	//Funcion en la cual se hace el ordenamiento de notas de mayor a menor "bsort" 
	int id_aux;
	
	for(int h = 0; h < (cant_notas - 1); h++){
		for(int k = 0; k < (cant_notas - h - 1); k++){
			if(curso[id[k]].prom < curso[id[k + 1]].prom){
				id_aux = id[k];
				id[k] = id[k + 1];
				id[k + 1] = id_aux;
			}
		}
	}
}

void guardarAprobadosyReprobados(FILE *file, int id[], int cant_notas, estudiante curso[]){	//funcion en la cual se guardan en el archivo las notas ya sea de aprobados o reprobados basandose en la cantidad de aprob/reprob y en el "id del alumno" en esa condicion
	int id_a = 0, i;

	while(id_a < cant_notas){
		i = id[id_a];
		fprintf(file,"%s\t%s\t%s\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t %.1f\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM, curso[i].asig_1.proy1, curso[i].asig_1.proy2, curso[i].asig_1.proy3, 
			curso[i].asig_1.cont1, curso[i].asig_1.cont2, curso[i].asig_1.cont3, curso[i].asig_1.cont4, curso[i].asig_1.cont5, curso[i].asig_1.cont6, curso[i].prom);
		id_a++;
	}
}

void clasificarEstudiantes(char aprobados[], char reprobados[], estudiante curso[]){	//funcion en donde se clasifican los alumnos aprobados y los reprobados
	if(validar_carga == 1){	
		FILE *file;
		int id[const_array], id_a = 0, i = 0, cant_notas = 0;


		if((file=fopen(aprobados,"w"))==NULL){	//Se crea un archivo para los aprobados
			printf("\nerror al crear el archivo");
			exit(0); 
		}
		else{
			for (int i = 0; i < const_array; i++){	//Se recorre el arreglo

				if(curso[i].prom >= 4.0){		//SI la nota es mayor o igual a cuatro se guarda la id del alumno y se agrega uno a la cantidad de notas
					id[cant_notas] = i;
					cant_notas++;				
				}
			}

			ordenarNotas(id, cant_notas, curso);	//Se procede a ordenar las notas
			guardarAprobadosyReprobados(file, id, cant_notas, curso);	//y se guardan en el archivo
		} 
		printf("\n **El registro de estudiantes aprobados fue almacenado de manera correcta**\n \n ");
		fclose(file);


		if((file=fopen(reprobados,"w"))==NULL){	//Se realiza el mismo procedimiento para los reprobados
			printf("\nerror al crear el archivo");
			exit(0); 
		}
		else{
			cant_notas = 0;
			for (int i = 0; i < const_array; i++){
				if(curso[i].prom < 4.0){	//Con la condicion obvia de que sean notas menores a 4
					id[cant_notas] = i;
					cant_notas++;	
				}
			}

			ordenarNotas(id, cant_notas, curso);
			guardarAprobadosyReprobados(file, id, cant_notas, curso);
		} 
		printf("\n **El registro de estudiantes reprobados fue almacenado de manera correcta**\n \n ");
		fclose(file);

	}else{
		
		printf("\nCargue el archivo de estudiantes en la opcion 1.\n");
	}
}

void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					validar_carga = 1;
					break;

			case 2: registroCurso(curso);// Realiza ingreso de notas en el registro curso 				
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: 
					if(validar_carga == 1){
						grabarEstudiantes("test.txt",curso);
						break;
					}else{
						printf("\nCargue el archivo de estudiantes en la opcion 1.\n");
						break;
					}
            case 5: clasificarEstudiantes("aprobados.txt", "reprobados.txt", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}